/**
 * Extract images from splash.obj, raw.
 * 
 * This file is in the public domain.
 * Contributors: Sylvain Beucler
 */
#include <stdio.h>
extern "C" char splash[];
extern "C" char hidden[];
extern int splash_size;
extern int hidden_size;
int main(void) {
	printf("splash: %p\n", splash);
	printf("hidden: %p\n", hidden);
	printf("splash_size: %d\n", splash_size);
	printf("hidden_size: %d\n", hidden_size);
	FILE* out;
	out = fopen("splash", "wb");
	printf("%d\n", fwrite(splash, 1, splash_size, out));
	fclose(out);
	out = fopen("hidden", "wb");
	printf("%d\n", fwrite(hidden, 1, hidden_size, out));
	fclose(out);
}
/**
 * Local Variables:
 * compile-command: "i686-pc-mingw32-g++ extract.cpp splash.obj"
 * End:
 */
