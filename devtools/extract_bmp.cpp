/**
 * Extract images from splash.obj, as BMP.
 * 
 * This file is in the public domain.
 * Contributors: Sylvain Beucler
 */
#include "SDL.h"
extern char splash[];
extern char hidden[];
int main(int argc, char* argv[]) {
  SDL_Init(SDL_INIT_VIDEO);
  SDL_Event e;
  SDL_Surface* screen = SDL_SetVideoMode(512, 256, 24, SDL_DOUBLEBUF);

  SDL_Surface* surf;
  surf = SDL_CreateRGBSurfaceFrom((void*)splash,
				  512, 256, 24,
				  512*3, // pitch
				  0x000000ff, 0x0000ff00, 0x00ff0000, 0x00000000);
  if (surf == NULL) exit(1);
  SDL_SaveBMP(surf, "splash.bmp");

  SDL_FillRect(screen, NULL, (Uint32)-1);
  SDL_BlitSurface(surf, NULL, screen, NULL);
  SDL_Flip(screen);
  while (SDL_WaitEvent(&e)) {
    if (e.type == SDL_QUIT)
      break;
  }


  surf = SDL_CreateRGBSurfaceFrom((void*)hidden,
				  512, 256, 24,
				  512*3, // pitch
				  0x000000ff, 0x0000ff00, 0x00ff0000, 0x00000000);
  if (surf == NULL) exit(1);
  SDL_SaveBMP(surf, "hidden.bmp");

  SDL_FillRect(screen, NULL, (Uint32)-1);
  SDL_BlitSurface(surf, NULL, screen, NULL);
  SDL_Flip(screen);
  while (SDL_WaitEvent(&e)) {
    if (e.type == SDL_QUIT)
      break;
  }
  return 0;
}
/**
 * Local Variables:
 * compile-command: "i686-pc-mingw32-g++ extract_bmp.cpp splash.obj $(/usr/src/mxe/usr/i686-pc-mingw32/bin/sdl-config --cflags --libs)"
 * End:
 */
