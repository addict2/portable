/**
 * 3 methods to print a float's binary content
 */
#include <stdio.h>
int main(void) {
	{
		union { float f; unsigned int i; } u = { .f = 1.1f };

		printf("%02hhx%02hhx%02hhx%02hhx\n", ((char*)&u.f)[3], ((char*)&u.f)[2], ((char*)&u.f)[1], ((char*)&u.f)[0]);

		printf("%x\n", u.i);

		printf("%x\n", *(unsigned int*)&u.f);
		float f = .3f;
		printf("%x\n", *(unsigned int*)&f);
	}
}
