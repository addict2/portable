/**
 * Exhibit inconsistency in GCC floating point arithmetic, not present
 * in VC++2010.
 */
#include <stdio.h>
#include <stdlib.h>
#include <math.h>

int main(int argc, char* argv[]) {
	float a = atof(argv[1]); // try with 0x3fc90fdb (from 4players_release) or just '1'
	float t = 1/3.0f;
	float c = sin(t*a)/sin(a);
	float t1 = t*a;
	float t2 = sin(t1);
	float t3 = sin(a);
	float t4 = t2/t3;

	printf("c  = %08x | %.10f\n", *(unsigned int*)&c , c );
	// with a=1: GCC-O0: 3ec71597, GCC-O2: 3ec71597, VC++: 3ec71598
	printf("t4 = %08x | %.10f\n", *(unsigned int*)&t4, t4);
	// with a=1: GCC-O0: 3ec71598, GCC-O2: 3ec71597, VC++: 3ec71598

	// printf("t = %08x t*a=%08x sin(t*a)=%08x sin(a)=%08x\n",
	//        *(unsigned int*)&t, *(unsigned int*)&t1, *(unsigned int*)&t2, *(unsigned int*)&t3);
}
