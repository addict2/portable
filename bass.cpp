/**
 * BASS stub
 */
#include "bass.h"
extern int BASS_Init(int i1, int hz, int i2, HWND hWnd, char*) { return 1; }
extern void BASS_ChannelSetPosition(HSTREAM str, float pos) {}
extern void BASS_ChannelStop(HSTREAM str) {}
extern HSTREAM BASS_StreamCreateFile(BOOL b, char*, int i1, int i2, int i3) { return 0; }
extern void BASS_StreamFree(HSTREAM str) {}
extern float BASS_StreamGetLength(HSTREAM) { return 0; }
extern void BASS_StreamPlay(HSTREAM str, BOOL b, int) {}
extern float BASS_ChannelBytes2Seconds(HSTREAM str, float pos) { return 0; }
extern float BASS_ChannelSeconds2Bytes(HSTREAM str, float pos) { return 0; }
extern float BASS_ChannelGetPosition(HSTREAM) { return 0; }
