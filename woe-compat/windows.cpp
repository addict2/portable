#include <sys/time.h>
#include <unistd.h>

/**
 * Naive implementation for kernel32's timeGetTime.
 *
 * Not bothering with monotonic clocks since we're running
 * minutes-long demos.
 */
unsigned int timeGetTime() {
	struct timeval tv;
	gettimeofday(&tv, NULL);
	return tv.tv_sec*1000 + tv.tv_usec;
}

void Sleep(unsigned int ms) {
	usleep(ms*1000);
}
