/**
 * MS Woe compatibility layer

 * Copyright (C) 2013  Sylvain Beucler

 * This file is part of aDDict2

 * aDDict2 is free software; you can redistribute it and/or modify it
 * under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation; either version 3 of the License, or
 * (at your option) any later version.

 * aDDict2 is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * General Public License for more details.

 * You should have received a copy of the GNU Affero General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/>.
 */

#ifndef WOE_H
#define WOE_H

#ifndef _WIN32

#include </usr/include/SDL/SDL.h>
typedef SDL_Surface* HDC;
typedef SDL_Surface* HWND;
typedef SDL_Cursor* HCURSOR;
typedef SDL_Surface* HICON;

#define WINAPI
#define CALLBACK
#define __stdcall
#  define __int64 long long
typedef int BOOL;
typedef long LRESULT;
typedef struct dirent* WIN32_FIND_DATA;
typedef struct tagPOINT {
    long x;
    long y;
} POINT;

typedef struct tagMSG {
    HWND hwnd;
    unsigned int message;
    unsigned int wParam;
    long long lParam;
    unsigned int time;
    POINT pt;
} MSG;
typedef unsigned int UINT;
typedef unsigned int WPARAM;
typedef long long LPARAM;

#define max(a,b) ((a)>(b)?(a):(b))
#define min(a,b) ((a)<(b)?(a):(b))

unsigned int timeGetTime();
void Sleep(unsigned int ms);

#endif

#endif
