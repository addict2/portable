#pragma once
#include "GUIinterface.h"

void __stdcall GUI_3dLeftClick(cINTERFACE* Interface, int ButtonId);
void DisplayRotateTool(GUIITEM *View);
void DisplayMoveTool(GUIITEM *View);
void DisplayScaleTool(GUIITEM *View);
