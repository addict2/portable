#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <time.h>
#include <mmintrin.h>

#define SIZE 16

union RGBA
{
	unsigned char c[4];
	struct { unsigned char r,g,b,a; };
	unsigned int dw;
};


static void __stdcall operator_cpp_add(RGBA*Old, RGBA*New,int opsize) {
	for (int i = 0; i < opsize; i++)
		Old[i].r += New[i].r;
}

static void __stdcall operator_asm_add(RGBA*Old, RGBA*New,int opsize) {
	/*
	__asm {
		mov ecx,opsize
		mov esi,New
		mov edi,Old
		addloop:
			movd mm0,[edi]
			movd mm1,[esi]
			paddusb mm0,mm1
			movd eax,mm0
			stosb
			add edi,3
			add esi,4
		loop addloop
		emms
	}
	*/
	asm volatile (
		"1:                     \n\t"
		"  movd (%%edi), %%mm0  \n\t"
		"  movd (%%esi), %%mm1  \n\t"
		"  paddusb %%mm1, %%mm0 \n\t"
		"  movd %%mm0, %%eax    \n\t"
		"  stosb                \n\t"	/* Move byte al->edi, edi++ */
		"  add $3, %%edi        \n\t"
		"  add $4, %%esi        \n\t"
		"loop 1b                \n\t"
		"emms                   \n\t"
		: "+D" (Old),		/* load Old address into edi, modified by the loop */
		  "+S" (New),		/* load New address into esi, modified by the loop */
		  "+c" (opsize)		/* load loop counter (opsize) into ecx, modified by the loop */
		:
		: "memory", "eax"	/* *Old is modified; eax is modified */
	);
}

void __stdcall operator_cpp_sel(RGBA*Old, RGBA*New, int opsize) {
	for (int i = 0; i < opsize; i++)
		Old[i].r = New[i].r;
}

void __stdcall operator_asm_sel(RGBA*Old, RGBA*New,int opsize) {
	/*
	__asm {
		mov ecx,opsize
		mov esi,New
		mov edi,Old
		selloop:
			movsb
			add edi,3
			add esi,3
		loop selloop
	}
	*/
	asm volatile (
		"1:                     \n\t"
		"  movsb                \n\t"	/* Move byte esi->edi, esi++, edi++ */
		"  add $3, %%edi        \n\t"
		"  add $3, %%esi        \n\t"
		"loop 1b                \n\t"
		: "+D" (Old),		/* load Old address into edi, modified by the loop */
		  "+S" (New),		/* load New address into esi, modified by the loop */
		  "+c" (opsize)		/* load loop counter (opsize) into ecx, modified by the loop */
		:
		: "memory"		/* *Old is modified */
	);
}

void line() {
	printf("     -----------------------------------------------\n");
}

void compare(unsigned char* ASMOld, unsigned char* CPPOld, int size) {
	if (memcmp(ASMOld, CPPOld, size)==0)
		printf("OK\n");
	else
		printf("FAIL!\n");
}

int main(void) {
	srand(time(NULL));
	unsigned char* ASMOld = (unsigned char*) malloc(SIZE);
	unsigned char* CPPOld = (unsigned char*) malloc(SIZE);
	unsigned char* New = (unsigned char*) malloc(SIZE);
	for (int i = 0; i < SIZE; i++) {
		ASMOld[i] = CPPOld[i] = rand()%255;
		New[i] = rand()%255;
	}
	printf("add: ");
	for (int i = 0; i < SIZE; i++)
		printf("%02x ", ASMOld[i]);
	printf("\n");
	printf("     ");
	for (int i = 0; i < SIZE; i++)
		printf("%02x ", New[i]);
	printf("\n");
	line();

	operator_asm_add((RGBA*)ASMOld, (RGBA*)New, SIZE/4);
	operator_cpp_add((RGBA*)CPPOld, (RGBA*)New, SIZE/4);

	printf("     ");
	for (int i = 0; i < SIZE; i++)
		printf("%02x ", ASMOld[i]);
	printf("\n");
	printf("     ");
	for (int i = 0; i < SIZE; i++)
		printf("%02x ", CPPOld[i]);
	printf("\n");
	compare(ASMOld, CPPOld, SIZE);
	free(ASMOld);
	free(CPPOld);
	free(New);
}

/**
 * Local Variables:
 * compile-command: "i686-pc-mingw32-g++ -I.. -g -O2 -fdata-sections -ffunction-sections -mmmx asm.cpp -o asm.exe -lm -lopengl32 -lglu32 -lwinmm -lgdi32 -lole32 -loleaut32 -luuid"
 * End:
 */
