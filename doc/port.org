* Files removed
rm bass.h bass.lib bass.dll glew.h glext.h mvx.h mvxWavPlayerLib.lib wglew.h Gui/splash.obj
#rm tlFont.cpp tlFont.h
* Compile
** GNU/Linux (32bit-only for now)
mkdir native/
cd native/
../configure CXXFLAGS="-g -O2 -m32"
make -j4
./aDDict2
** mingw32
mkdir cross-woe/
cd cross-woe/
PATH=/usr/src/mxe/usr/bin:$PATH
../configure --host=i686-pc-mingw32
make -j4
wine aDDict2.exe
